#!/usr/bin/env python
from PyQt4 import QtCore, QtGui
from ui.gui import Ui_MainWindow
from ui.gui import _translate

import cv2
CV_VERS = float(cv2.__version__[0:3])
if CV_VERS >= 3.0:
    from QStereo3 import QStereoBM3 as QStereoBM
    from QStereo3 import QStereoSGBM3 as QStereoSGBM
else:
    from QStereo24 import QStereoSGBM24 as QStereoSGBM

import numpy as np
from scipy.misc import toimage as npToImage


CV_VERS = float(cv2.__version__[0:3])


class StereoApp(QtGui.QMainWindow, Ui_MainWindow):
    """
    Attributes:
        disparity (numpy.ndarray): Description
        file (str): Description
        imageReady (bool): Description
        imgMode (int): Description
        imgs (list): Description
        mode (int): Description
        stereo (QStereoAbstract): Description
    Examples:

        >>> import sys
        >>> app = QtGui.QApplication(sys.argv)
        >>> form = StereoApp()
        >>> form.show()
        >>> app.exec_()

    """

    def __init__(self):
        super(self.__class__, self).__init__()
        # initialzie parameter
        self.imageReady = False
        self.disparity = None
        self.mode = 0

        layout = self.setupUi(self)
        if CV_VERS >= 3.0:
            self.stereo = [QStereoSGBM("SGBM", self), QStereoBM("BM", self)]
            layout.addWidget(self.stereo[0])
            layout.addWidget(self.stereo[1])
            self.stereo[0].show()
            self.stereo[1].hide()
        else:
            self.stereo = [QStereoSGBM("SGBM", self)]
            layout.addWidget(self.stereo[0])
            self.stereo[0].show()
            self.cbMode.hide()
            self.lbMode.hide()

        # create GUI
        self._retranslateUi()
        self._connect()
        self.OnReset()
        self.resize(1200, 900)
        self.printTostatus("Please open an image file.")

    def _connect(self):
        self.btOpen.clicked.connect(self.OnOpen)
        self.btReset.clicked.connect(self.OnReset)
        self.btSave.clicked.connect(self.OnSave)
        self.cbImgMode.currentIndexChanged.connect(self.OnImg)
        self.stereo[0].valueChanged.connect(self.updateDisparity)
        if CV_VERS >= 3.0:
            self.cbMode.currentIndexChanged.connect(self.OnMode)
            self.stereo[1].valueChanged.connect(self.updateDisparity)

    def _retranslateUi(self):
        self.setWindowTitle(_translate("MainWindow", "qStereo", None))
        self.lbMode.setText(_translate("MainWindow", "Mode:", None))
        self.lbImgMode.setText(_translate("MainWindow", "Image:", None))
        self.btOpen.setText(_translate("MainWindow", "Open", None))
        self.btSave.setText(_translate("MainWindow", "Save", None))
        self.btReset.setText(_translate("MainWindow", "Reset", None))

    def updateDisparity(self):
        if self.imageReady:
            self.printTostatus("Calculate disparity map...")
            self.disparity = self.stereo[self.mode].compute(
                self.imgs[0], self.imgs[1])
            dmin = self.disparity.min()
            dmax = self.disparity.max()
            if dmax == dmin:
                return
            self._ndisparity = np.round(
                (self.disparity - dmin).astype(dtype=np.float) /
                (dmax - dmin) * 255).astype(dtype=np.uint8)
            # show disparity
            self.dscene.clear()
            h, w = self._ndisparity.shape
            img = QtGui.QImage(
                self._ndisparity.data, w, h, QtGui.QImage.Format_Indexed8)
            self.dscene.clear()
            self.dscene.addPixmap(QtGui.QPixmap.fromImage(img))
            self.dGPviewer.fitInView(0, 0, w, h, QtCore.Qt.KeepAspectRatio)
            self.dscene.update()
            self.printTostatus("Disparity map calculated.")

    def OnImg(self, imgMode):
        # create image preview
        self.imgMode = imgMode
        h, w = self.imgs[imgMode].shape
        img = QtGui.QImage(
            self.imgs[imgMode].data, w, h, QtGui.QImage.Format_Indexed8)
        self.oscene.clear()
        self.oscene.addPixmap(QtGui.QPixmap.fromImage(img))
        self.oGPviewer.fitInView(0, 0, w, h, QtCore.Qt.KeepAspectRatio)
        self.oscene.update()

    def OnOpen(self):
        self.dscene.clear()
        self.file = [str(QtGui.QFileDialog.getOpenFileName(
            self.centralwidget,
            directory='./image',
            caption=_translate("MainWindow", "Open file", None)))]
        # check file exist
        if self.file[0] is None:
            self.printTostatus("File does not exist. Please load image.")
        else:
            if QtGui.QMessageBox.question(
                    self.centralwidget,
                    _translate("MainWindow", "", None),
                    _translate("MainWindow", "Open another image?", None),
                    buttons=QtGui.QMessageBox.Yes | QtGui.QMessageBox.No
            ) == QtGui.QMessageBox.Yes:
                self.file.append(
                    str(QtGui.QFileDialog.getOpenFileName(
                        self.centralwidget,
                        directory='./image',
                        caption=_translate("MainWindow", "Open file", None))))
                if self.file[1] is None or self.file[1] == "":
                    self.file = [self.file[0]]

            if len(self.file) == 1:
                img = cv2.imread(self.file[0], 0)
                # check image format
                if img is None:
                    QtGui.QMessageBox.warning(
                        self.centralwidget,
                        _translate("MainWindow", "Format Error.", None),
                        _translate("MainWindow",
                                   "Can't read image file.", None))
                    return

                self.imgs = [np.copy(img[:, :img.shape[1] / 2]),
                             np.copy(img[:, img.shape[1] / 2:])]
            else:
                self.imgs[0] = cv2.imread(self.file[0], 0)
                self.imgs[1] = cv2.imread(self.file[1], 0)
                if self.imgs[0] is None or self.imgs[1] is None:
                    QtGui.QMessageBox.warning(
                        self.centralwidget,
                        _translate("MainWindow", "Format Error.", None),
                        _translate("MainWindow",
                                   "Can't read image file.", None))
                    return

            self.imageReady = True
            self.OnImg(0)
            # calculate disparity
            self.updateDisparity()

    def OnMode(self, mode):
        self.mode = mode
        if mode:
            self.stereo[1].show()
            self.stereo[0].hide()
        else:
            self.stereo[0].show()
            self.stereo[1].hide()
        self.OnReset()

    def OnReset(self):
        self.stereo[self.mode].reset()
        self.updateDisparity()

    def OnSave(self):
        if self.disparity is not None:
            sfile = str(QtGui.QFileDialog.getSaveFileName(
                self.centralwidget,
                directory='./image',
                caption=_translate("MainWindow", "Save file", None)))
            simg = npToImage(self.disparity,
                             cmin=form.disparity.min(),
                             cmax=form.disparity.max())
            simg.save(sfile)
            self.printTostatus("File saved.")
        else:
            QtGui.QMessageBox.warning(
                self.centralwidget,
                _translate("MainWindow", "Format Error.", None),
                _translate("MainWindow",
                           "Please load image first.", None))

    def resizeEvent(self, event):
        QtGui.QWidget.resizeEvent(self, event)
        if self.imageReady:
            h, w = self.imgs[self.imgMode].shape
            self.oscene.update()
            self.oGPviewer.fitInView(0, 0, w, h, QtCore.Qt.KeepAspectRatio)
            h, w = self.disparity.shape
            self.dGPviewer.fitInView(0, 0, w, h, QtCore.Qt.KeepAspectRatio)
            self.dscene.update()

    def printTostatus(self, msg):
        self.statusbar.showMessage(str(msg))

if __name__ == '__main__':
    import sys
    app = QtGui.QApplication(sys.argv)  # A new instance of QApplication
    form = StereoApp()
    form.show()                         # Show the form
    app.exec_()
