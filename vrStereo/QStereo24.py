from QStereoBase import QStereoAbstract, _fromUtf8, QHSliderGroup
from collections import OrderedDict
from cv2 import StereoSGBM


class QStereoSGBM24(QStereoAbstract):
    """
    This QFrame class create StereoSGBM to compute disparity from two images
    by OpenCV 2.4 or later. This widget provides a group of QSlider to setup
    the parameters of stereo engine.

    Note:
        This class won't support OpenCV 3.0 or later. Please use QStereoSGBM3
        instead.

    Attributes:
        engine (cv2.StereoBM): Stereo engine using SGBM by OpenCV
        params (OrderedDict): Parameters required by StereoBM and QSlider.
        This Dict is defined as follow,

        parameeters name: (label name, default value, step, minimum, maximum)

    Examples:
        Initialize this widget after any Qt Application,

        >>> stereo = QStereoSGBM24("SGBM")

        Use reset method to initialize the parameters of engine,

        >>> stereo.reset()

        Then you can comput the disparity map of two images,

        >>> disparity = stereo.compute(img1, img2)

        This class is a QFrame, and it can be shown as follow,

        >>> stereo.show()

    """

    def __init__(self, name, parent=None):
        super(QStereoSGBM24, self).__init__(parent)
        # set setereo params and egine
        self.params = OrderedDict([
            ("minDisparity", ("Min Disparity", -64, 1, -128, 128)),
            ("numberOfDisparities", ("# Disparities", 192, 16, 16, 256)),
            ("SADWindowSize", ("Block Size", 5, 2, 5, 255)),
            ("disp12MaxDiff", ("Disp12MaxDiff", 10, 1, 1, 100)),
            ("uniquenessRatio", ("Uniqueness Ratio", 1, 1, 1, 20)),
            ("preFilterCap", ("PreFilter Cap", 4, 1, 1, 63)),
            ("speckleRange", ("Speckle Range", 2, 1, 1, 20)),
            ("speckleWindowSize", ("Speckle Window Size", 150, 1, 0, 200))
        ])
        self.engine = StereoSGBM(0, 0, 0)

        # create ui
        self.setObjectName(_fromUtf8(name))
        self.initControl()

    def reset(self):
        for name, (label, value, interval, MIN, MAX) in self.params.items():
            slider = self.findChild(QHSliderGroup, "SG" + name)
            slider.setValue(value)
            self.engine.__setattr__(name, value)

    def OnValueChanged(self, value):
        name = str(self.sender().objectName())[2:]
        self.engine.__setattr__(name, value)
        if name == "SADWindowSize":
            self.engine.P1(24 * value ** 2)
            self.engine.P2(96 * value ** 2)
        self.valueChanged.emit()
