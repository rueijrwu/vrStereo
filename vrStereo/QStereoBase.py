from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

import numpy as np


class QHSliderGroup(QtGui.QSlider):
    """
    Attributes:
        layout (QLayout): The layout object of this class
        nameLabel (QLabel): Description
        step (int): Description
        ValueChanged (): Description
        valueLabel (QLabel): Description
    """
    ValueChanged = QtCore.pyqtSignal(int)

    def __init__(self, parent=None):
        """init method

        Args:
            parent (QWidget, optional): parent of this class
        """
        super(self.__class__, self).__init__(
            QtCore.Qt.Horizontal, parent)
        self.layout = QtGui.QHBoxLayout()
        self.nameLabel = QtGui.QLabel(parent)
        self.valueLabel = QtGui.QLabel(parent)
        self.layout.addWidget(self.nameLabel)
        self.layout.addWidget(self)
        self.layout.addWidget(self.valueLabel)
        self.setSretch(0, 3)
        self.setSretch(1, 9)
        self.setSretch(2, 1)
        self.valueChanged.connect(self._redirectValueChanged)
        self.step = 1

    def setStep(self, value):
        """Set step size of slider, page and mouse wheel

        Args:
            value (int): Postion of QSlider
        """
        self.step = value
        self.setSingleStep(value)
        self.setPageStep(value)

    def setObjectName(self, name):
        self.layout.setObjectName(_fromUtf8("SGLayout" + name))
        self.nameLabel.setObjectName(_fromUtf8("SGName" + name))
        super(self.__class__, self).setObjectName(_fromUtf8("SG" + name))
        self.valueLabel.setObjectName(_fromUtf8("SGValue" + name))

    def setSretch(self, index, stretch):
        self.layout.setStretch(index, stretch)

    def getLayout(self):
        return self.layout

    def setText(self, text):
        self.nameLabel.setText(text)

    def setValue(self, value):
        super(self.__class__, self).setValue(value)
        self.valueLabel.setText(str(value))

    def _redirectValueChanged(self, value):
        rValue = (value - self.minimum()) % self.step
        if rValue:
            self.setValue(value - rValue)
        else:
            self.setValue(value)
            self.ValueChanged.emit(value)


class QStereoAbstract(QtGui.QFrame):
    valueChanged = QtCore.pyqtSignal()

    def __init__(self, parent=None):
        super(QStereoAbstract, self).__init__(parent=parent)

        # set setereo params and egine
        self.params = {}
        self.engine = None

    def initControl(self):
        self.layout = QtGui.QVBoxLayout()
        for k, (n, value, interval, MIN, MAX) in self.params.items():
            obj = QHSliderGroup(self)
            obj.setObjectName(k)
            obj.setTracking(False)
            obj.setRange(MIN, MAX)
            obj.setTickInterval(interval)
            obj.setStep(interval)
            obj.setValue(value)
            obj.ValueChanged.connect(self.OnValueChanged)
            obj.setText(_translate(self.objectName(), n, None))
            self.layout.addLayout(obj.getLayout())
        self.setLayout(self.layout)

    def OnValueChanged(self, value):
        pass

    def compute(self, img1, img2):
        if not isinstance(img1, np.ndarray):
            raise TypeError("The type of img1 is not numpy.ndarray")
        if not isinstance(img2, np.ndarray):
            raise TypeError("The type of img2 is not numpy.ndarray")
        if not (img1.shape == img2.shape):
            raise ValueError(
                "all the input array dimensions must match exactly")
        if self.engine:
            return self.engine.compute(img1, img2)
