# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'stereo.ui'
#
# Created: Wed Sep 30 14:36:29 2015
#      by: PyQt4 UI code generator 4.10.4
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class QHSliderGroup(QtGui.QSlider):
    ValueChanged = QtCore.pyqtSignal(int)

    def __init__(self, parent=None):
        super(self.__class__, self).__init__(
            QtCore.Qt.Horizontal, parent)
        self.layout = QtGui.QHBoxLayout()
        self.nameLabel = QtGui.QLabel(parent)
        self.valueLabel = QtGui.QLabel(parent)
        self.layout.addWidget(self.nameLabel)
        self.layout.addWidget(self)
        self.layout.addWidget(self.valueLabel)
        self.setSretch(0, 3)
        self.setSretch(1, 9)
        self.setSretch(2, 1)
        self.valueChanged.connect(self._redirectValueChanged)
        self.sliderStep = 1

    def setStep(self, value):
        self.step = value
        self.setSingleStep(value)
        self.setPageStep(value)

    def setObjectName(self, name):
        self.layout.setObjectName(_fromUtf8("SGLayout" + name))
        self.nameLabel.setObjectName(_fromUtf8("SGName" + name))
        super(self.__class__, self).setObjectName(_fromUtf8("SG" + name))
        self.valueLabel.setObjectName(_fromUtf8("SGValue" + name))

    def setSretch(self, index, stretch):
        self.layout.setStretch(index, stretch)

    def getLayout(self):
        return self.layout

    def setText(self, text):
        self.nameLabel.setText(text)

    def setValue(self, value):
        super(self.__class__, self).setValue(value)
        self.valueLabel.setText(str(value))

    def _redirectValueChanged(self, value):
        rValue = (value - self.minimum) % self.step
        if rValue:
            self.setValue(value - rValue)
        else:
            self.setValue(value)
            self.ValueChanged.emit(value)


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1185, 785)

        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        layout1 = QtGui.QHBoxLayout(self.centralwidget)
        layout1.setObjectName(_fromUtf8("layout1"))

        self.dscene = QtGui.QGraphicsScene()
        self.dscene.setBackgroundBrush(QtCore.Qt.black)
        self.dGPviewer = QtGui.QGraphicsView(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(
            self.dGPviewer.sizePolicy().hasHeightForWidth())
        self.dGPviewer.setSizePolicy(sizePolicy)
        self.dGPviewer.setObjectName(_fromUtf8("dGPviewer"))
        self.dGPviewer.setScene(self.dscene)
        layout1.addWidget(self.dGPviewer)

        layout2 = QtGui.QVBoxLayout()
        layout2.setObjectName(_fromUtf8("layout2"))
        layout1.addLayout(layout2)

        self.oscene = QtGui.QGraphicsScene()
        # self.oscene.setBackgroundBrush(QtCore.Qt.black)
        self.oGPviewer = QtGui.QGraphicsView(self.centralwidget)
        sizePolicy = QtGui.QSizePolicy(
            QtGui.QSizePolicy.Expanding, QtGui.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        sizePolicy.setHeightForWidth(
            self.oGPviewer.sizePolicy().hasHeightForWidth())
        self.oGPviewer.setSizePolicy(sizePolicy)
        self.oGPviewer.setObjectName(_fromUtf8("oGPviewer"))
        self.oGPviewer.setScene(self.oscene)
        layout2.addWidget(self.oGPviewer)

        layout3 = QtGui.QHBoxLayout()
        layout3.setObjectName(_fromUtf8("layout3"))
        layout2.addLayout(layout3)

        self.lbMode = QtGui.QLabel(self.centralwidget)
        self.lbMode.setObjectName(_fromUtf8("lbMode"))
        layout3.addWidget(self.lbMode)

        self.cbMode = QtGui.QComboBox(self.centralwidget)
        self.cbMode.setObjectName(_fromUtf8("cbMode"))
        self.cbMode.addItem("SGBM")
        self.cbMode.addItem("BM")
        layout3.addWidget(self.cbMode)

        self.lbImgMode = QtGui.QLabel(self.centralwidget)
        self.lbImgMode.setObjectName(_fromUtf8("lbImgMode"))
        layout3.addWidget(self.lbImgMode)

        self.cbImgMode = QtGui.QComboBox(self.centralwidget)
        self.cbImgMode.setObjectName(_fromUtf8("cbImgMode"))
        self.cbImgMode.addItem("Left")
        self.cbImgMode.addItem("Right")
        layout3.addWidget(self.cbImgMode)

        layoutSlider = QtGui.QVBoxLayout()
        layoutSlider.setObjectName(_fromUtf8("layoutSlider"))
        layout2.addLayout(layoutSlider)

        layout5 = QtGui.QHBoxLayout()
        layout5.setObjectName(_fromUtf8("layout5"))
        layout2.addLayout(layout5)

        self.btOpen = QtGui.QPushButton(self.centralwidget)
        self.btOpen.setObjectName(_fromUtf8("btOpen"))
        layout5.addWidget(self.btOpen)

        self.btSave = QtGui.QPushButton(self.centralwidget)
        self.btSave.setObjectName(_fromUtf8("btSave"))
        layout5.addWidget(self.btSave)

        self.btReset = QtGui.QPushButton(self.centralwidget)
        self.btReset.setObjectName(_fromUtf8("btReset"))
        layout5.addWidget(self.btReset)

        layout1.setStretch(0, 2)
        layout1.setStretch(1, 1)
        layout2.setStretch(0, 1)
        layout3.setStretch(0, 1)
        layout3.setStretch(1, 2)
        layout3.setStretch(2, 1)
        layout3.setStretch(3, 2)

        MainWindow.setCentralWidget(self.centralwidget)

        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)

        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        return layoutSlider
