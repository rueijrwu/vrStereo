from QStereoBase import *
from QStereoBase import _fromUtf8
from collections import OrderedDict
from cv2 import StereoBM_create, StereoSGBM_create


class QStereoSGBM3(QStereoAbstract):
    """
    This QFrame class create StereoSGBM to compute disparity from two images
    by OpenCV. This widget provides a group of QSlider to setup the parameters
    of stereo engine.

    Note:
        This class requires OpenCV 3.0 or later. Use StereoSGBM24 for
        OpenCV 2.4.

    Attributes:
        engine (cv2.StereoBM): Stereo engine using SGBM by OpenCV
        params (OrderedDict): Parameters required by StereoBM and QSlider.
        This Dict is defined as follow,

        parameeters name: (label name, default value, step, minimum, maximum)

    Examples:
        Initialize this widget after any Qt Application,

        >>> stereo = QStereoSGBM3("SGBM")

        Use reset method to initialize the parameters of engine,

        >>> stereo.reset()

        Then you can comput the disparity map of two images,

        >>> disparity = stereo.compute(img1, img2)

        This class is a QFrame, and it can be shown as follow,

        >>> stereo.show()

    """

    def __init__(self, name, parent=None):
        """ init method

        Args:
            name (string): The name of the class.
            parent (QWidget, optional): Parent of the class.
        """
        super(QStereoSGBM3, self).__init__(parent)
        # set setereo params and egine
        self.params = OrderedDict([
            ("MinDisparity", ("Min Disparity", -64, 1, -128, 128)),
            ("NumDisparities", ("# Dispairties", 192, 16, 16, 256)),
            ("BlockSize", ("Block Size", 5, 2, 5, 255)),
            ("Disp12MaxDiff", ("Disp12MaxDiff", 10, 1, 1, 100)),
            ("UniquenessRatio", ("Uniqueness Ratio", 1, 1, 1, 20)),
            ("PreFilterCap", ("PreFilter Cap", 4, 1, 1, 63)),
            ("SpeckleRange", ("Speckle Range", 2, 1, 1, 20)),
            ("SpeckleWindowSize", ("Speckle Window Size", 150, 1, 0, 200))
        ])
        self.engine = StereoSGBM_create(0, 0, 0)

        # create ui
        self.setObjectName(_fromUtf8(name))
        self.initControl()

    def reset(self):
        """ Reset method

        Use this method to he parameters of stereo engine and QSlider.

        Example:
            All parameters will be reset to default value.
            >>> stereo.reset()
        """
        for name, (label, value, interval, MIN, MAX) in self.params.items():
            slider = self.findChild(QHSliderGroup, "SG" + name)
            slider.setValue(value)
            self.engine.__getattribute__("set" + name)(value)

    def OnValueChanged(self, value):
        """Callback method when ValueChanged is emit from any QSlider

        This callback will set values from QSlider to stereo engine. This
        callback also emit valueChanged signal when finished.

        Args:
            value (int value): The position of QSlider

        """
        name = str(self.sender().objectName())[2:]
        fn = self.engine.__getattribute__("set" + name)
        if fn:
            fn(value)
            if name == "BlockSize":
                self.engine.setP1(24 * value ** 2)
                self.engine.setP2(96 * value ** 2)
        self.valueChanged.emit()


class QStereoBM3(QStereoAbstract):
    """
    This QFrame class creates StereoBM Engine to compute disparity from two
    images by OpenCV 3.0 or later. This widget also provides a group of QSlider
    to setup the parameters of stereo engine.

    Note:
        This class requires OpenCV 3.0 or later.

    Args:
        name (string): The name of this class.
        parent (QWidget, optional): Parent of this class.

    Attributes:
        engine (cv2.StereoBM): Stereo engine using SGBM by OpenCV
        params (OrderedDict): Parameters required by StereoBM and QSlider.
        This Dict is defined as follow,

        parameeters name: (label name, default value, step, minimum, maximum)

    Examples:
        Initialize this widget after any Qt Application,

        >>> stereo = QStereoBM3("BM")

        Use reset method to initialize the parameters of engine,

        >>> stereo.reset()

        Then you can comput the disparity map of two images,

        >>> disparity = stereo.compute(img1, img2)

        This class is a QFrame, and it can be shown as follow,

        >>> stereo.show()

    """

    def __init__(self, name, parent=None):
        """ init method
        Args:
            name (string): The name of the class.
            parent (QWidget, optional): Parent of the class.
        """
        super(QStereoBM3, self).__init__(parent)
        # set setereo params and egine
        self.params = OrderedDict([
            ("MinDisparity", ("Min Disparity", -39, 1, -128, 128)),
            ("NumDisparities", ("# Disparities", 112, 16, 16, 256)),
            ("BlockSize", ("Block Size", 9, 2, 5, 255)),
            ("Disp12MaxDiff", ("Disp12MaxDiff", 1, 1, 1, 100)),
            ("UniquenessRatio", ("Uniqueness Ratio", 1, 1, 1, 20)),
            ("PreFilterCap", ("PreFilter Cap", 61, 1, 1, 63)),
            ("PreFilterSize", ("PreFilter Size", 5, 2, 5, 255)),
            ("SpeckleRange", ("Speckle Range", 8, 1, 1, 20)),
            ("SpeckleWindowSize", ("Speckle Window Size", 0, 1, 0, 200)),
            ("TextureThreshold", ("Texture Threshold", 507, 1, 1, 1000))
        ])
        self.engine = StereoBM_create()

        # create ui
        self.setObjectName(_fromUtf8(name))
        self.initControl()

    def reset(self):
        """ reset
        Use this method to he parameters of stereo engine and QSlider.

        Example:
            All parameters will be reset to default value.
            >>> stereo.reset()
        """
        for name, (label, value, interval, MIN, MAX) in self.params.items():
            slider = self.findChild(QHSliderGroup, "SG" + name)
            slider.setValue(value)
            self.engine.__getattribute__("set" + name)(value)

    def OnValueChanged(self, value):
        """callback method when ValueChanged is emit from any QSlider

        This callback will set values from QSlider to stereo engine. This
        callback also emit valueChanged signal when finished.

        Args:
            value (int value): The position of QSlider

        """
        name = str(self.sender().objectName())[2:]
        fn = self.engine.__getattribute__("set" + name)
        if fn:
            fn(value)
        self.valueChanged.emit()
